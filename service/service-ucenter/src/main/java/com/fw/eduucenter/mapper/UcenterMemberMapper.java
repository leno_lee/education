package com.fw.eduucenter.mapper;

import com.fw.eduucenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-17
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {
    //查询某天的注册人数
    Integer countRegister(String day);
}
