package com.fw.eduucenter.controller;


import com.fw.commonutils.R;
import com.fw.commonutils.UcenterMemberVo;
import com.fw.eduucenter.entity.UcenterMember;
import com.fw.eduucenter.entity.vo.RegisterVo;
import com.fw.eduucenter.service.UcenterMemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.swing.*;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-17
 */
@RestController
@RequestMapping("/eduucenter/member")
@CrossOrigin
public class UcenterMemberController {
    @Autowired
    private UcenterMemberService memberService;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    //登录
    @PostMapping("login")
    public R loginUser(@RequestBody UcenterMember member){
        //调用业务方法
        //返回token值
        String token = memberService.login(member);
        return R.ok().data("token",token);
    }
    //注册
    @PostMapping("register")
    public R register(@RequestBody RegisterVo registerVo){
        memberService.register(registerVo,redisTemplate);
        return R.ok();
    }
    //根据token获得用户信息
    @GetMapping("getLoginInfo")
    public R getLoginInfo(HttpServletRequest request){
        UcenterMember member = memberService.getLoginInfo(request);
        return R.ok().data("member",member);
    }

    //课程评论、订单获得用户信息
    @GetMapping("getUserInfo/{id}")
    public UcenterMemberVo getUserInfo(@PathVariable("id") String id){
        UcenterMember user = memberService.getById(id);
        UcenterMemberVo userInfo = new UcenterMemberVo();
        // System.out.println(user);
        BeanUtils.copyProperties(user,userInfo);
        return userInfo;
    }
    //某天注册人数
    @GetMapping("registerCount/{day}")
    public R registerCount(@PathVariable("day") String day){
        Integer count = memberService.registerCount(day);
        return R.ok().data("count",count);
    }
}

