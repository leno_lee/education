package com.fw.eduucenter.service;

import com.fw.eduucenter.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.eduucenter.entity.vo.RegisterVo;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-17
 */
public interface UcenterMemberService extends IService<UcenterMember> {
    //登录，返回token值
    String login(UcenterMember member);
    //注册
    void register(RegisterVo registerVo, RedisTemplate<String,String> redisTemplate);
    //根据token获得用户信息
    UcenterMember getLoginInfo(HttpServletRequest request);
    //根据openid查询用户信息
    UcenterMember getMemberByOpenid(String openid);
    //某天的注册人数
    Integer registerCount(String day);
}
