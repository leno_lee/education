package com.fw.eduucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.commonutils.JwtUtils;
import com.fw.commonutils.MD5;
import com.fw.eduucenter.entity.UcenterMember;
import com.fw.eduucenter.entity.vo.RegisterVo;
import com.fw.eduucenter.mapper.UcenterMemberMapper;
import com.fw.eduucenter.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.servicebase.exception.GuliException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-17
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {


    //登录，返回token值
    @Override
    public String login(UcenterMember member) {
        //手机号不能为空
        String phone = member.getMobile();
        String password = member.getPassword();
        if (StringUtils.isEmpty(phone)){
            throw new GuliException(20001,"手机号不能为空");
        }
        //密码不能为空
        if (StringUtils.isEmpty(password)){
            throw new GuliException(20001,"密码不能为空");
        }
        /*
        if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)){
            throw new GuliException(20001,"error");
        }
         */
        //根据输入的手机号查询数据
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",phone);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);
        //数据不能为空
        if (null == ucenterMember){
            throw new GuliException(20001,"没有该用户");
        }
        //判断密码是否正确
        //数据库中的密码是经过MD5加密
        if (!MD5.encrypt(password).equals(ucenterMember.getPassword())){
            throw new GuliException(20001,"密码不正确");
        }
        //判断该用户是否被禁用
        if (ucenterMember.getIsDisabled()){
            throw new GuliException(20001,"该用户已被禁用");
        }

        //构建token值
        return JwtUtils.getJwtToken(ucenterMember.getId(),ucenterMember.getNickname());
    }

    //注册
    @Override
    public void register(RegisterVo registerVo, RedisTemplate<String,String> redisTemplate) {
        //参数不能为空
        String nickname = registerVo.getNickname();
        String mobile = registerVo.getMobile();
        String password = registerVo.getPassword();
        String code = registerVo.getCode();
        if (StringUtils.isEmpty(nickname) || StringUtils.isEmpty(mobile)
            || StringUtils.isEmpty(password) || StringUtils.isEmpty(code)){
            throw new GuliException(20001,"参数不能为空");
        }

        //从Redis中获得缓存的验证码
        String redisCode = redisTemplate.opsForValue().get(mobile);
        if (StringUtils.isEmpty(redisCode)){
            throw new GuliException(20001,"验证码已失效");
        }
        //判断验证码是否相同
        if (!code.equals(redisCode)){
            throw new GuliException(20001,"验证码错误");
        }
        //判断数据库中是否已有该手机号的用户
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if (count > 0){
            throw new GuliException(20001,"该手机号已注册过");
        }
        //将用户信息写入数据库
        UcenterMember member = new UcenterMember();
        member.setNickname(nickname);
        member.setMobile(mobile);
        member.setPassword(MD5.encrypt(password));//经过MD5加密后传入数据库
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");

        baseMapper.insert(member);
    }
    //根据token获得用户信息
    @Override
    public UcenterMember getLoginInfo(HttpServletRequest request) {
        //获得id
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //查询该用户
        UcenterMember member = baseMapper.selectById(memberId);

        return member;
    }

    //根据openid查询用户信息
    @Override
    public UcenterMember getMemberByOpenid(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid",openid);
        UcenterMember member = baseMapper.selectOne(wrapper);
        return member;
    }

    //某天注册人数
    @Override
    public Integer registerCount(String day) {
        //查询某天的注册人数
        Integer count = baseMapper.countRegister(day);
        return count;
    }
}
