package com.fw.eduucenter.controller;

import com.fw.commonutils.JwtUtils;
import com.fw.eduucenter.entity.UcenterMember;
import com.fw.eduucenter.service.UcenterMemberService;
import com.fw.eduucenter.utils.ConstantPropertiesUtil;
import com.fw.eduucenter.utils.HttpClientUtils;
import com.fw.servicebase.exception.GuliException;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

@Controller
@CrossOrigin
@RequestMapping("/api/ucenter/wx")
public class WxApiController {

    @Autowired
    private UcenterMemberService memberService;
    //回调函数
    @GetMapping("callback")
    public String callback(String code,String state){

        try {
            //获得code值，临时票据
            // System.out.println(code);
            // System.out.println(state); //原样返回

            //通过HttpClient像微信提供的固定地址发送请求获得access_token:访问凭证,openid：每个微信唯一标识
            //构建固定地址
            String accessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token" +
                    "?appid=%s" +
                    "&secret=%s" +
                    "&code=%s" +
                    "&grant_type=authorization_code";
            accessTokenUrl = String.format(
                    accessTokenUrl,
                    ConstantPropertiesUtil.WX_OPEN_APP_ID,
                    ConstantPropertiesUtil.WX_OPEN_APP_SECRET,
                    code  //临时票据
            );
            String result = HttpClientUtils.get(accessTokenUrl);
            // System.out.println(result);
            Gson gson = new Gson();
            HashMap map = gson.fromJson(result, HashMap.class);
            String access_token = (String)map.get("access_token");
            String openid = (String) map.get("openid");

            //判断该微信用户是否已注册过
            UcenterMember member = memberService.getMemberByOpenid(openid);
            if (member == null){
                //根据access_token和openid访问另一个地址获得用户信息
                String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" +
                        "?access_token=%s" +
                        "&openid=%s";
                baseUserInfoUrl = String.format(
                        baseUserInfoUrl,
                        access_token,
                        openid
                );
                String resultUserInfo = HttpClientUtils.get(baseUserInfoUrl);
                // System.out.println(resultUserInfo);

                //存入数据库
                member = new UcenterMember();
                HashMap<String,Object> userInfoMap = gson.fromJson(resultUserInfo,HashMap.class);
                String nickname = (String) userInfoMap.get("nickname");
                Double sex = (Double) userInfoMap.get("sex");
                String headimgurl = (String) userInfoMap.get("headimgurl");

                member.setNickname(nickname);
                member.setOpenid(openid);
                member.setAvatar(headimgurl);
                member.setSex(sex.intValue());
                memberService.save(member);
            }
            //token值
            String token = JwtUtils.getJwtToken(member.getId(), member.getNickname());
            //从地址栏中传递给前端
            return "redirect:http://localhost:3000?token="+token;
        } catch (Exception e) {
            throw new GuliException(20001,"微信登录失败");
        }
    }

    //微信二维码登录
    @GetMapping("login")
    public String wxLogin(){
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=STATE" +
                "#wechat_redirect ";
        //编码获得重定向uri
        String redirectUrl = ConstantPropertiesUtil.WX_OPEN_REDIRECT_URL;
        try {
            redirectUrl=URLEncoder.encode(redirectUrl,"utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new GuliException(20001,e.getMessage());
        }

        //生成qrcodeUrl
        String qrcodeUrl = String.format(
                baseUrl,
                ConstantPropertiesUtil.WX_OPEN_APP_ID,
                redirectUrl
        );
        return "redirect:"+qrcodeUrl;
    }
}
