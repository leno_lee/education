package com.fw.eduservice;

import com.alibaba.excel.EasyExcel;
import com.fw.eduservice.entities.DemoData;
import org.junit.Test;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class TestEasyExcel {


    @Test
    public void testWrite(){
        //设置写入文件的路径
        String filename="G:\\1.xlsx";
        //
        EasyExcel.write(filename, DemoData.class).sheet("学生列表").doWrite(getList());
    }

    public static List<DemoData> getList(){
        List<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 10 ; i++) {
            DemoData demoData = new DemoData();
            demoData.setSid(""+i);
            demoData.setSname("lucy"+i);
            list.add(demoData);
        }
        return list;
    }
    //读操作
    @Test
    public void  testRead(){
        String filename="G:\\1.xlsx";
        EasyExcel.read(filename,DemoData.class,new ExcelListener()).sheet().doRead();
    }
}
