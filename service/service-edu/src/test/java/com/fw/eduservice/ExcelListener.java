package com.fw.eduservice;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.fw.eduservice.entities.DemoData;

import java.util.Map;

public class ExcelListener extends AnalysisEventListener<DemoData> {

    //一行一行的读
    @Override
    public void invoke(DemoData data, AnalysisContext context) {
        System.out.println("******"+data);
    }

    //读取表头信息
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头信息："+headMap);
    }

    //读取完成之后
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
