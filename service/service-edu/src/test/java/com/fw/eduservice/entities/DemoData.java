package com.fw.eduservice.entities;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class DemoData {

    //设置表头名称
    @ExcelProperty(value = "学生编号",index = 0)
    private String sid;
    @ExcelProperty(value = "学生姓名",index = 1)
    private String sname;
}
