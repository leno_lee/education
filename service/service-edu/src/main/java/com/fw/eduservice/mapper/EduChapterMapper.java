package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
