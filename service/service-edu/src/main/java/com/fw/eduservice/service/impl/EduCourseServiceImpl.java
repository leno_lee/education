package com.fw.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.eduservice.client.OrderClient;
import com.fw.eduservice.entity.EduCourse;
import com.fw.eduservice.entity.EduCourseDescription;
import com.fw.eduservice.entity.EduTeacher;
import com.fw.eduservice.entity.vo.*;
import com.fw.eduservice.mapper.EduCourseMapper;
import com.fw.eduservice.service.EduChapterService;
import com.fw.eduservice.service.EduCourseDescriptionService;
import com.fw.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.eduservice.service.EduVideoService;
import com.fw.servicebase.exception.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService courseDescriptionService;

    @Autowired
    private EduVideoService videoService;

    @Autowired
    private EduChapterService chapterService;

    //添加课程
    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        //涉及到两张表：课程表还有课程简介表
        //首先将课程部分添加进课程表中
        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,course);
        boolean save = this.save(course);
        if (!save){
            throw new GuliException(20001,"添加课程信息失败");
        }
        //课程与课程简介是一对一的关系，简介的ID应该是课程的id值
        String cid = course.getId();

        //再将简介部分添加进课程简介表中
        EduCourseDescription description = new EduCourseDescription();

        description.setId(cid);
        description.setDescription(courseInfoVo.getDescription());
        boolean resultDescription = courseDescriptionService.save(description);
        if (!resultDescription){
            throw new GuliException(20001,"添加课程详细信息失败");
        }
        return cid;
    }


    //修改课程基本信息
    @Override
    public String updateCourseById(CourseInfoVo courseInfoVo) {
        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,course);
        int update = baseMapper.updateById(course);
        if (update == 0){
            throw new GuliException(20001,"修改课程基本信息失败");
        }
        String cid = course.getId();
        EduCourseDescription description = new EduCourseDescription();
        description.setId(cid);
        description.setDescription(courseInfoVo.getDescription());
        boolean save = courseDescriptionService.updateById(description);
        if (!save){
            throw new GuliException(20001,"修改课程详细信息事变");
        }
        return cid;
    }

    //根据课程id查询课程
    @Override
    public CourseInfoVo getCourseById(String courseId) {
        //获得课程部分
        // System.out.println(courseId);
        EduCourse eduCourse = baseMapper.selectById(courseId);
        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(eduCourse,courseInfoVo);

        //获得课程描述
        EduCourseDescription courseDescription = courseDescriptionService.getById(courseId);
        courseInfoVo.setDescription(courseDescription.getDescription());
        //封装数据
        return courseInfoVo;
    }
    //根据课程id查询课程确认信息
    @Override
    public CoursePublishVo publishCourseInfo(String id) {
        CoursePublishVo coursePublish = baseMapper.getCoursePublish(id);
        return coursePublish;
    }
    //根据id发布课程
    @Override
    public void publishCourseById(String id) {
        EduCourse course = new EduCourse();
        course.setId(id);
        course.setStatus("Normal");
        baseMapper.updateById(course);
    }

    //根据课程删除课程
    @Override
    public void deleteCourseById(String id) {
        //1、根据课程id删除小节
        videoService.removeVideoByCourseId(id);
        //2、根据课程id删除章节
        chapterService.deleteChapterByCourseId(id);
        //3、根据课程id删除描述
        courseDescriptionService.removeById(id);
        //4、根据课程id删除课程
        int result = baseMapper.deleteById(id);
        if (result == 0){
            throw new GuliException(20001,"删除课程失败");
        }
    }

    //课程的条件查询分页
    @Override
    public Map<String, Object> getFrontCourse(Page<EduCourse> params, CourseQueryVo courseQueryVo) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        //拼接条件
        if (!StringUtils.isEmpty(courseQueryVo.getSubjectParentId())){
            wrapper.eq("subject_parent_id",courseQueryVo.getSubjectParentId());
        }
        if (!StringUtils.isEmpty(courseQueryVo.getSubjectId())){
            wrapper.eq("subject_id",courseQueryVo.getSubjectId());
        }
        if (!StringUtils.isEmpty(courseQueryVo.getBuyCountSort())){//关注度
            wrapper.orderByDesc("buy_count",courseQueryVo.getBuyCountSort());
        }
        if (!StringUtils.isEmpty(courseQueryVo.getGmtCreateSort())){//最新
            wrapper.orderByDesc("gmt_create",courseQueryVo.getGmtCreateSort());
        }
        if (!StringUtils.isEmpty(courseQueryVo.getPriceSort())){//价格
            wrapper.orderByDesc("price",courseQueryVo.getPriceSort());
        }
        baseMapper.selectPage(params,wrapper);

        //获得分页后的数据
        long total = params.getTotal();
        List<EduCourse> courses = params.getRecords();
        long current = params.getCurrent();
        long size = params.getSize();
        long pages = params.getPages();
        boolean hasNext = params.hasNext();
        boolean hasPrevious = params.hasPrevious();

        //创建一个map集合，并将数据放入其中
        Map<String,Object> map = new HashMap<>();
        map.put("total",total);
        map.put("courses",courses);
        map.put("current",current);
        map.put("size",size);
        map.put("pages",pages);
        map.put("hasNext",hasNext);
        map.put("hasPrevious",hasPrevious);

        return map;

    }

    //根据课程id获得课程详情页面
    @Override
    public CourseFrontInfoVo getFrontCourseInfoByCourseId(String courseId) {
        CourseFrontInfoVo courseFrontInfoVo = baseMapper.getFrontCourseInfoByCourseId(courseId);
        return courseFrontInfoVo;
    }

}
