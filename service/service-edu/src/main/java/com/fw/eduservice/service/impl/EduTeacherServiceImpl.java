package com.fw.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.eduservice.entity.EduTeacher;
import com.fw.eduservice.mapper.EduTeacherMapper;
import com.fw.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-09-21
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

    //查询所有讲师，在名师栏显示
    @Override
    public Map<String, Object> getFrontTeacher(Page<EduTeacher> params) {
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        baseMapper.selectPage(params,wrapper);

        //获得params中的数据
        long total = params.getTotal();
        List<EduTeacher> teachers = params.getRecords();
        long current = params.getCurrent();
        long size = params.getSize();
        long pages = params.getPages();
        boolean hasNext = params.hasNext();
        boolean hasPrevious = params.hasPrevious();

        //创建一个map集合，并将数据放入其中
        Map<String,Object> map = new HashMap<>();
        map.put("total",total);
        map.put("teachers",teachers);
        map.put("current",current);
        map.put("size",size);
        map.put("pages",pages);
        map.put("hasNext",hasNext);
        map.put("hasPrevious",hasPrevious);


        return map;
    }
}
