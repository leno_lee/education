package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-09-21
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
