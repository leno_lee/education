package com.fw.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.commonutils.R;
import com.fw.eduservice.entity.Comment;
import com.fw.eduservice.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Wrapper;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/eduservice/comment")
@CrossOrigin
public class CommentController {

    @Autowired
    private CommentService commentService;

    //1、分页查询课程评论
    @GetMapping("{page}/{limit}/{courseId}")
    public R getCommentList(@PathVariable("page") long page,
                            @PathVariable("limit") long limit,
                            @PathVariable("courseId") String courseId){
        Page<Comment> commentPage = new Page<>(page,limit);
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        wrapper.orderByDesc("gmt_create");
        commentService.page(commentPage,wrapper);

        Map<String,Object> map = new HashMap<>();
        map.put("commentList",commentPage.getRecords());
        map.put("total",commentPage.getTotal());
        map.put("current",commentPage.getCurrent());
        map.put("size",commentPage.getSize());
        map.put("pages",commentPage.getPages());
        map.put("hasNext",commentPage.hasNext());
        map.put("previous",commentPage.hasPrevious());
        return R.ok().data(map);
    }

    //2、添加评论
    @PostMapping("addComment")
    public R addComment(@RequestBody Comment comment, HttpServletRequest request){
        commentService.addComment(comment,request);
        return R.ok();
    }
}

