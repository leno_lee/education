package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.eduservice.entity.vo.CourseFrontInfoVo;
import com.fw.eduservice.entity.vo.CoursePublishVo;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {
    //根据课程id获得课程发布信息
    CoursePublishVo getCoursePublish(String courseId);
    //根据课程id获得课程详情页面
    CourseFrontInfoVo getFrontCourseInfoByCourseId(String courseId);
}
