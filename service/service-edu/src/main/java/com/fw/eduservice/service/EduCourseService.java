package com.fw.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.eduservice.entity.vo.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduCourseService extends IService<EduCourse> {
    //添加课程基本信息
    String saveCourseInfo(CourseInfoVo courseInfoVo);
    //修改课程基本信息
    String updateCourseById(CourseInfoVo courseInfoVo);
    //根据课程id查询课程
    CourseInfoVo getCourseById(String courseId);
    //根据课程id查询课程确认信息
    CoursePublishVo publishCourseInfo(String id);
    //根据id发布课程
    void publishCourseById(String id);
    //根据课程ID删除课程
    void deleteCourseById(String id);
    //课程的条件查询分页
    Map<String, Object> getFrontCourse(Page<EduCourse> params, CourseQueryVo courseQueryVo);
    //根据课程id获得课程详情页面
    CourseFrontInfoVo getFrontCourseInfoByCourseId(String courseId);

}
