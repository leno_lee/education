package com.fw.eduservice.service.impl;

import com.fw.commonutils.JwtUtils;
import com.fw.commonutils.UcenterMemberVo;
import com.fw.eduservice.client.UcenterClient;
import com.fw.eduservice.entity.Comment;
import com.fw.eduservice.entity.EduTeacher;
import com.fw.eduservice.mapper.CommentMapper;
import com.fw.eduservice.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.eduservice.service.EduTeacherService;
import com.fw.servicebase.exception.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Autowired
    private UcenterClient ucenterClient;

    @Autowired
    private EduTeacherService teacherService;

    //添加评论
    @Override
    public void addComment(Comment comment, HttpServletRequest request) {
        //获得token里的用户id
        String id = JwtUtils.getMemberIdByJwtToken(request);
        // System.out.println(id);
        //获得用户信息
        UcenterMemberVo userInfo = ucenterClient.getUserInfo(id);
        BeanUtils.copyProperties(userInfo,comment);
        //添加评论信息
        int insert = baseMapper.insert(comment);
        if (insert == 0){
            throw new GuliException(20001,"插入评论失败");
        }
    }
}
