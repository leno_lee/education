package com.fw.eduservice.controller;


import com.fw.commonutils.R;
import com.fw.eduservice.entity.EduVideo;
import com.fw.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@RestController
@RequestMapping("/eduservice/video")
@CrossOrigin
public class EduVideoController {
    @Autowired
    private EduVideoService videoService;
    /**
     * 添加小节
     */
    @PostMapping("addVideo")
    public R addVideo(@RequestBody EduVideo video){
        videoService.save(video);
        return R.ok();
    }

    /**
     * 删除小节
     * @param id 小节id
     * @return 成功的信息
     */
    @DeleteMapping("deleteVideo/{id}")
    public R deleteVideo(@PathVariable("id") String id){
        videoService.removeVideoById(id);
        return R.ok();
    }
    //根据id获取小节
    @GetMapping("getVideo/{id}")
    public R getVideo(@PathVariable("id") String id){
        EduVideo eduVideo = videoService.getById(id);
        // System.out.println(eduVideo.getIsFree());
        return R.ok().data("video",eduVideo);
    }

    //修改小节
    @PostMapping("updateVideo")
    public R updateVideo(@RequestBody EduVideo video){
        videoService.updateById(video);
        return R.ok();
    }

}

