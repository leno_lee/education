package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
