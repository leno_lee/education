package com.fw.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.commonutils.R;
import com.fw.eduservice.entity.EduTeacher;
import com.fw.eduservice.entity.vo.TeacherQuery;
import com.fw.eduservice.service.EduTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-09-21
 */
@Api(description = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class EduTeacherController {

    @Autowired
    private EduTeacherService eduTeacherService;

    /*
    1、查询所有老师数据
     */
    @ApiOperation(value = "查询所有讲师列表")
    @GetMapping("findAll")
    public R findAllTeacher(){
        List<EduTeacher> list = eduTeacherService.list(null);
        return R.ok().data("items",list);
    }

    /*
    2、逻辑删除
     */
    @ApiOperation(value = "根据id删除讲师")
    @DeleteMapping("{id}")
    public R removeById(
            @ApiParam(name = "id", value = "讲师ID", required = true)
            @PathVariable("id") String id){
        boolean flag = eduTeacherService.removeById(id);
        if ( flag ){
            return R.ok();
        }else {
            return R.error();
        }
    }

    /*
    3、分页查询
     */
    @ApiOperation(value = "分页讲师列表")
    @GetMapping("{page}/{limit}")
    public R pageList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable("page") Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable("limit") Long limit){

        Page<EduTeacher> pageParam = new Page<>(page, limit);

        eduTeacherService.page(pageParam, null);
        List<EduTeacher> records = pageParam.getRecords();
        long total = pageParam.getTotal();

        return  R.ok().data("total", total).data("rows", records);
    }

    /*
    4、条件查询带分页的方法
     */
    @ApiOperation(value = "条件查询讲师并且带有分页功能")
    @PostMapping(value = "pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition (
            @ApiParam(name = "current",value = "当前页",required = true)
            @PathVariable("current") Long current,
            @ApiParam(name = "limit",value = "每页记录数",required = true)
            @PathVariable("limit") Long limit,
            @RequestBody(required = false) TeacherQuery teacherQuery){
        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(current, limit);

        //调用方法实现条件查询
        QueryWrapper wrapper = new QueryWrapper();
        //多条件组合查询
        //判断条件值是否为空，如果不为空拼接条件
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();

        if (!StringUtils.isEmpty(name)){
            //构建条件
            wrapper.like("name",name);
        }
        if (!StringUtils.isEmpty(level)){
            //构建条件
            wrapper.eq("level",level);
        }
        if (!StringUtils.isEmpty(begin)){
            //构建条件
            wrapper.ge("gmt_create",begin);//前面用数据库中的列名
        }
        if (!StringUtils.isEmpty(end)){
            //构建条件
            wrapper.le("gmt_create",end);
        }

        //排序
        wrapper.orderByDesc("gmt_create");

        eduTeacherService.page(pageTeacher, wrapper);

        long total = pageTeacher.getTotal();
        List<EduTeacher> records = pageTeacher.getRecords();

        return  R.ok().data("total", total).data("rows", records);
    }


    /**
     * 5、添加讲师
     * @param eduTeacher 被添加的讲师的属性实体类
     * @return
     */
    @ApiOperation(value = "添加讲师")
    @PostMapping(value = "addTeacher")
    public R addTeacher(@RequestBody EduTeacher eduTeacher){
        boolean save = eduTeacherService.save(eduTeacher);
        if (save){
            return R.ok();
        }else {
            return R.error();
        }
    }

    /**
     * 6、根据id查询讲师
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询讲师")
    @GetMapping(value = "getTeacher/{id}")
    public R addTeacher(@PathVariable String id ){
        EduTeacher teacher = eduTeacherService.getById(id);
        return R.ok().data("teacher",teacher);
    }

    /**
     * 6、修改讲师
     * @param eduTeacher
     * @return
     */
    @ApiOperation(value = "修改讲师")
    @PostMapping(value = "updateTeacher")
    public R updateTeacher(@RequestBody EduTeacher eduTeacher){
        boolean update = eduTeacherService.updateById(eduTeacher);
        if (update){
            return R.ok();
        }else {
            return R.error();
        }
    }


}

