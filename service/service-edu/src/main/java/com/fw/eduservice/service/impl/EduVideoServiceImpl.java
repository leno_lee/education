package com.fw.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.commonutils.R;
import com.fw.eduservice.client.VodClient;
import com.fw.eduservice.entity.EduVideo;
import com.fw.eduservice.mapper.EduVideoMapper;
import com.fw.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.servicebase.exception.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {
    @Autowired
    private VodClient vodClient;

    //删除小节的同时删除视频
    @Override
    public void removeVideoById(String id) {
        //先删除云端的视频
        EduVideo video = baseMapper.selectById(id);
        //云端视频的id
        String videoId = video.getVideoSourceId();
        // System.out.println(videoId);
        //调用服务端的方法
        if (!StringUtils.isEmpty(videoId)){
            R result = vodClient.deleteAlyiVideo(videoId);
            if (result.getCode()==20001){
                throw new GuliException(20001,"删除视频失败，熔断器");
            }
        }
        //删除小节
        baseMapper.deleteById(id);
    }

    //根据课程id删除小节
    @Override
    public void removeVideoByCourseId(String courseId) {
        QueryWrapper<EduVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id",courseId);
        queryWrapper.select("video_source_id");

        //查询该课程所有的小节视频
        List<EduVideo> videos = baseMapper.selectList(queryWrapper);

        List<String> videoIds = new ArrayList<>();
        for (EduVideo video : videos) {
            String videoSourceId = video.getVideoSourceId();
            if (!StringUtils.isEmpty(videoSourceId)){
                videoIds.add(videoSourceId);
            }
        }
        //删除视频
        if (videoIds.size()>0){
            vodClient.deleteAlyListVideo(videoIds);
        }
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);

    }
}
