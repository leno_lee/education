package com.fw.eduservice.service;

import com.fw.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduVideoService extends IService<EduVideo> {
    //删除小节的同时删除视频
    void removeVideoById(String id);

    //根据课程id删除小节
    void removeVideoByCourseId(String courseId);
}
