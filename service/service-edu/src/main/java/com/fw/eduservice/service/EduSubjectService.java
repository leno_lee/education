package com.fw.eduservice.service;

import com.fw.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.eduservice.entity.subject.OneSubject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author fw
 * @since 2020-09-30
 */
public interface EduSubjectService extends IService<EduSubject> {

    void saveSubject(MultipartFile file,EduSubjectService subjectService);

    List<OneSubject> getList();
}
