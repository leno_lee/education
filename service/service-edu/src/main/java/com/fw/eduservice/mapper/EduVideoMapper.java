package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
