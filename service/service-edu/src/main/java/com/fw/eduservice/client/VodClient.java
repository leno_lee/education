package com.fw.eduservice.client;

import com.fw.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(name = "service-vod",fallback = VodFileDegradeFeignClient.class)
public interface VodClient {
    //删除某个阿里云视频
    @DeleteMapping("/eduvod/video/deleteAlyiVideo/{videoId}")
    public R deleteAlyiVideo(@PathVariable("videoId") String videoId);
    //删除多个阿里云视频
    @DeleteMapping("/eduvod/video/deleteAlyListVideo")
    public R deleteAlyListVideo(@RequestParam("videoIdList") List<String> videoIdList);
}
