package com.fw.eduservice.service.impl;

import com.fw.eduservice.entity.EduCourseDescription;
import com.fw.eduservice.mapper.EduCourseDescriptionMapper;
import com.fw.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
