package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
