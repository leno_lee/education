package com.fw.eduservice.client;


import com.fw.commonutils.R;
import com.fw.commonutils.UcenterMemberVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;


@Component
@FeignClient(name = "service-ucenter",fallback = UcenterFeignClient.class)
public interface UcenterClient {
    //课程评论获得用户信息
    @GetMapping("/eduucenter/member/getUserInfo/{id}")
    public UcenterMemberVo getUserInfo(@PathVariable("id") String id);
}
