package com.fw.eduservice.service;

import com.fw.eduservice.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
public interface CommentService extends IService<Comment> {
    //添加评论
    void addComment(Comment comment, HttpServletRequest request);
}
