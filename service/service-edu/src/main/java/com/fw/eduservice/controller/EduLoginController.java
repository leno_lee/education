package com.fw.eduservice.controller;

import com.fw.commonutils.R;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/eduservice/user")
@CrossOrigin
public class EduLoginController {

    //login
    @ApiOperation(value = "用户登录")
    @PostMapping("login")
    public R login(){
       return R.ok().data("token","admin");
    }

    //info
    @ApiOperation(value = "用户信息")
    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","admin").data("name","admin").data("avatar","avatar");
    }


}
