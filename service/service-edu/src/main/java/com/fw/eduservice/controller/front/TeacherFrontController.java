package com.fw.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.commonutils.R;
import com.fw.eduservice.entity.EduCourse;
import com.fw.eduservice.entity.EduTeacher;

import com.fw.eduservice.service.EduCourseService;
import com.fw.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/eduservice/teacherfront")
public class TeacherFrontController {

    @Autowired
    private EduTeacherService teacherService;
    @Autowired
    private EduCourseService courseService;
    //1、名师列表
    @GetMapping("getTeacherList/{page}/{limit}")
    public R getTeacherList(@PathVariable("page") long page,
                            @PathVariable("limit") long limit){
        Page<EduTeacher> params = new Page<>(page,limit);
        Map<String,Object> map = teacherService.getFrontTeacher(params);
        return R.ok().data(map);
    }
    //2、讲师详情
    @GetMapping("getTeacherInfo/{teacherId}")
    public R getTeacherInfo(@PathVariable("teacherId") String teacherId){
        //根据id查询讲师基本信息
        EduTeacher teacher = teacherService.getById(teacherId);
        //获得讲师所讲课程信息
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_id",teacherId);
        List<EduCourse> courseList = courseService.list(wrapper);
        return R.ok().data("teacher",teacher).data("courseList",courseList);
    }
}
