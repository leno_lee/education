package com.fw.eduservice.service;

import com.fw.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.eduservice.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
public interface EduChapterService extends IService<EduChapter> {
    //根据课程id查询课程大纲
    List<ChapterVo> getChapterVideoByCourseId(String courseId);

    //删除章节的方法
    boolean deleteChapter(String id);
    //根据课程id删除章节
    boolean deleteChapterByCourseId(String courseId);
}
