package com.fw.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.commonutils.CourseOrderInfoVo;
import com.fw.commonutils.JwtUtils;
import com.fw.commonutils.R;
import com.fw.eduservice.client.OrderClient;
import com.fw.eduservice.entity.EduCourse;
import com.fw.eduservice.entity.EduTeacher;
import com.fw.eduservice.entity.chapter.ChapterVo;
import com.fw.eduservice.entity.vo.CourseFrontInfoVo;
import com.fw.eduservice.entity.vo.CourseInfoVo;
import com.fw.eduservice.entity.vo.CourseQueryVo;
import com.fw.eduservice.service.EduChapterService;
import com.fw.eduservice.service.EduCourseService;
import com.fw.eduservice.service.EduVideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/eduservice/coursefront")
public class CourseFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrderClient orderClient;

    //1、条件查询分页课程
    @PostMapping("getFrontCourseList/{page}/{limit}")
    public R getFrontCourseList(@PathVariable("page") long page,
                                @PathVariable("limit") long limit,
                                @RequestBody(required = false) CourseQueryVo courseQueryVo){
        Page<EduCourse> params = new Page<>(page,limit);
        Map<String,Object> map = courseService.getFrontCourse(params,courseQueryVo);
        return R.ok().data(map);
    }
    //2、根据课程id课程详情页面
    @GetMapping("getCourseFrontInfo/{courseId}")
    public R getCourseFrontInfo(@PathVariable("courseId") String courseId,HttpServletRequest request){
        //根据课程id获得课程详情页面
        CourseFrontInfoVo courseFrontInfoVo = courseService.getFrontCourseInfoByCourseId(courseId);
        //根据课程id获得章节小节信息
        List<ChapterVo> chapterVideo = chapterService.getChapterVideoByCourseId(courseId);
        boolean isBuy = orderClient.isBuyCourse(courseId, JwtUtils.getMemberIdByJwtToken(request));
        return R.ok().data("courseFrontInfoVo",courseFrontInfoVo).data("chapterVideo",chapterVideo).data("isBuy",isBuy);
    }
    //3、订单里的课程信息
    @GetMapping("getOrderCourseInfo/{courseId}")
    public CourseOrderInfoVo getOrderCourseInfo(@PathVariable("courseId") String courseId){
        CourseInfoVo courseById = courseService.getCourseById(courseId);
        CourseOrderInfoVo courseOrderInfoVo = new CourseOrderInfoVo();
        BeanUtils.copyProperties(courseById,courseOrderInfoVo);
        return courseOrderInfoVo;
    }

}
