package com.fw.eduservice.controller;


import com.fw.commonutils.R;
import com.fw.eduservice.entity.EduChapter;
import com.fw.eduservice.entity.EduCourse;
import com.fw.eduservice.entity.chapter.ChapterVo;
import com.fw.eduservice.service.EduChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@RestController
@RequestMapping("/eduservice/chapter")
@CrossOrigin
public class EduChapterController {
    @Autowired
    private EduChapterService chapterService;

    //课程大纲列表
    @GetMapping("getChapterVideo/{courseId}")
    public R getChapterVideo(@PathVariable("courseId") String courseId){
        //根据课程id查询课程大纲
        List<ChapterVo> list = chapterService.getChapterVideoByCourseId(courseId);
        return  R.ok().data("chapterVideo",list);
    }

    /**
     * 添加章节
     */
    @PostMapping("addChapter")
    public R addChapter(@RequestBody EduChapter chapter){
        chapterService.save(chapter);
        return R.ok();
    }

    /**
     * 删除章节
     */
    @DeleteMapping("deleteChapter/{id}")
    public R deleteChapter(@PathVariable("id") String id){
        Boolean result = chapterService.deleteChapter(id);
        if (result){
            return R.ok();
        }else {
            return R.error().message("删除失败");
        }
    }

    /**
     * 根据课程id获得章节
     */
    @GetMapping("getChapter/{id}")
    public R getChapter(@PathVariable("id") String id){
        EduChapter chapter = chapterService.getById(id);
        return R.ok().data("chapterInfo",chapter);
    }

    /**
     * 修改章节
     */
    @PostMapping("updateChapter")
    public R updateChapter(@RequestBody EduChapter chapter){
        chapterService.updateById(chapter);
        return R.ok();
    }
}

