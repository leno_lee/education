package com.fw.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.commonutils.R;
import com.fw.eduservice.entity.EduCourse;
import com.fw.eduservice.entity.vo.CourseInfoVo;
import com.fw.eduservice.entity.vo.CoursePublishVo;
import com.fw.eduservice.entity.vo.CourseQuery;
import com.fw.eduservice.service.EduCourseService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@RestController
@RequestMapping("/eduservice/educourse")
@CrossOrigin
@Api(description = "课程管理")
public class EduCourseController {

    @Autowired
    private EduCourseService courseService;

    //课程列表
    //TODO 完善条件查询
    //条件查询课程
    @PostMapping("getCourseInfo/{page}/{limit}")
    public R getCourseInfo(@RequestBody(required = false) CourseQuery courseQuery,
                           @PathVariable("page") Long page,
                           @PathVariable("limit") Long limit){
        Page<EduCourse> coursePage = new Page<>(page,limit);
        //条件查询课程
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        String title = courseQuery.getTitle();
        String status = courseQuery.getStatus();

        if (!StringUtils.isEmpty(title)){
            wrapper.like("title",title);
        }
        if (!StringUtils.isEmpty(status)){
            wrapper.eq("status",status);
        }
        wrapper.orderByDesc("gmt_create");
        courseService.page(coursePage,wrapper);
        long total = coursePage.getTotal();
        List<EduCourse> courses = coursePage.getRecords();
        return R.ok().data("courses",courses).data("total",total);
    }

    @GetMapping
    public R getCourseList(){
        List<EduCourse> list = courseService.list(null);
        return R.ok().data("list",list);
    }

    //添加课程基本信息
    @PostMapping("addCourse")
    public R addCourse(@RequestBody CourseInfoVo courseInfoVo){
        String cid = courseService.saveCourseInfo(courseInfoVo);
        return R.ok().data("cid",cid);
    }

    //根据课程id查询课程
    @GetMapping("getCourse/{courseId}")
    public R getCourse(@PathVariable("courseId") String courseId){
        CourseInfoVo courseInfoVo= courseService.getCourseById(courseId);
        return R.ok().data("courseInfoVo",courseInfoVo);
    }


    //修改课程基本信息
    @PostMapping("updateCourse")
    public R updateCourse(@RequestBody CourseInfoVo courseInfoVo){
        //修改课程基本信息
        String cid = courseService.updateCourseById(courseInfoVo);
        return R.ok().data("cid",cid);
    }
    //根据课程id查询课程确认信息
    @GetMapping("getPublishCourseInfo/{id}")
    public R getPublishCourseInfo(@PathVariable("id") String id){
        CoursePublishVo coursePublishVo = courseService.publishCourseInfo(id);
        return R.ok().data("coursePublish",coursePublishVo);
    }
    //修改课程的status
    @GetMapping("publishCourse/{id}")
    public R publishCourse(@PathVariable("id") String id){
        courseService.publishCourseById(id);
        return R.ok();
    }
    //根据课程删除课程
    @DeleteMapping("deleteCourse/{id}")
    public R deleteCourse(@PathVariable("id") String id){
        courseService.deleteCourseById(id);
        return R.ok();
    }
}

