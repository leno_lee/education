package com.fw.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.eduservice.entity.EduSubject;
import com.fw.eduservice.entity.excel.SubjectData;
import com.fw.eduservice.entity.subject.OneSubject;
import com.fw.eduservice.entity.subject.TwoSubject;
import com.fw.eduservice.listener.SubjectExcelListener;
import com.fw.eduservice.mapper.EduSubjectMapper;
import com.fw.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.servicebase.exception.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-09-30
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {
    //添加课程分类
    @Override
    public void saveSubject(MultipartFile file,EduSubjectService subjectService) {
        try {
            //获得文件输入流
            InputStream in = file.getInputStream();
            //调用方法进行读取
            EasyExcel.read(in, SubjectData.class,new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
            throw new GuliException(20001,"添加课程分类失败");
        }
    }

    /**
     * 获得课程分类的列表
     * @return 树状的数据结构
     */
    @Override
    public List<OneSubject> getList() {
        //1、查询所有一级分类
        QueryWrapper<EduSubject> wrapperOne = new QueryWrapper<>();
        wrapperOne.eq("parent_id",'0');
        List<EduSubject> oneSubjectList = baseMapper.selectList(wrapperOne);

        //2、查询所有二级分类
        QueryWrapper<EduSubject> wrapperTwo = new QueryWrapper<>();
        wrapperTwo.ne("parent_id",'0');
        List<EduSubject> twoSubjectList = baseMapper.selectList(wrapperTwo);

        //新建一个集合接收封装后的数据
        List<OneSubject> list = new ArrayList<>();
        //3、封装
        for (EduSubject eduSubject : oneSubjectList) {
            OneSubject oneSubject = new OneSubject();
            //第一种方法，直接传值
            // oneSubject.setId(eduSubject.getId());
            // oneSubject.setTitle(eduSubject.getTitle());
            //第二种方法，使用工具类
            //此种传值，只传名字相同的属性值
            BeanUtils.copyProperties(eduSubject,oneSubject);

            //创建一个集合接收第二分类的课程
            List<TwoSubject> twoSubjects = new ArrayList<>();
            for (EduSubject subject : twoSubjectList) {
                if (subject.getParentId().equals(eduSubject.getId())){
                    TwoSubject twoSubject = new TwoSubject();
                    BeanUtils.copyProperties(subject,twoSubject);
                    twoSubjects.add(twoSubject);
                }
            }
            oneSubject.setChildren(twoSubjects);

            list.add(oneSubject);
        }

        return list;
    }
}
