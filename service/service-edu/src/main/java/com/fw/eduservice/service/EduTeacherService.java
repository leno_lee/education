package com.fw.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fw.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author fw
 * @since 2020-09-21
 */
public interface EduTeacherService extends IService<EduTeacher> {
    //查询所有讲师，在名师栏显示
    Map<String, Object> getFrontTeacher(Page<EduTeacher> params);
}
