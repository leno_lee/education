package com.fw.eduservice.controller;


import com.fw.commonutils.R;
import com.fw.eduservice.entity.subject.OneSubject;
import com.fw.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-09-30
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
@Api(description = "课程分类管理")
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;

    //添加课程分类
    //获取上传过来的文件，把文件内容读出来
    @ApiOperation(value = "添加课程列表")
    @PostMapping("addSubject")
    public R addSubject(MultipartFile file){
        //上传过来的文件
        eduSubjectService.saveSubject(file,eduSubjectService);
        return R.ok();
    }

    //获得课程分类列表
    @ApiOperation(value = "获得课程分类列表")
    @GetMapping("getSubjectList")
    public R getSubjectList(){
        List<OneSubject> list = eduSubjectService.getList();
        return R.ok().data("list",list);
    }
}

