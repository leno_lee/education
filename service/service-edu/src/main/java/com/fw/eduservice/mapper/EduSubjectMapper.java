package com.fw.eduservice.mapper;

import com.fw.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-09-30
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
