package com.fw.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.eduservice.entity.EduChapter;
import com.fw.eduservice.entity.EduVideo;
import com.fw.eduservice.entity.chapter.ChapterVo;
import com.fw.eduservice.entity.chapter.VideoVo;
import com.fw.eduservice.mapper.EduChapterMapper;
import com.fw.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.eduservice.service.EduVideoService;
import com.fw.servicebase.exception.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-02
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService videoService;

    //根据课程id查询课程大纲
    @Override
    public List<ChapterVo> getChapterVideoByCourseId(String courseId) {
        //1、根据课程id查询章节
        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id",courseId);
        List<EduChapter> chapterList = baseMapper.selectList(queryWrapper);

        //2、查询小节
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        List<EduVideo> videoList = videoService.list(wrapper);

        List<ChapterVo> finalList = new ArrayList<>();
        //3、将章节封装
        for (EduChapter chapter : chapterList) {
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(chapter,chapterVo);

            //4、封装小节
            List<VideoVo> videoVoList = new ArrayList<>();
            for (EduVideo video : videoList) {
                //判断chapter_id是否与当前章节id一样
                if (video.getChapterId().equals(chapter.getId())){
                    //获得相等的小节集合list
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(video,videoVo);
                    videoVoList.add(videoVo);
                }
            }
            chapterVo.setChildren(videoVoList);
            finalList.add(chapterVo);
        }
        return finalList;
    }

    /**
     * 根据是否有小节内容决定是否删除章节的方法
     * @param id 章节id
     * @return
     */
    @Override
    public boolean deleteChapter(String id) {
        //查询章节下是否有小节
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id",id);
        int count = videoService.count(wrapper);
        if (count>0){
            //查询出小节，不能删除
            throw new GuliException(20001,"不能删除");
        }else {
            //没有小节，可以删除
            int result = baseMapper.deleteById(id);
            return result>0;
        }
    }
    //根据课程id删除章节
    @Override
    public boolean deleteChapterByCourseId(String courseId) {
        //查询章节下是否有小节
        QueryWrapper<EduVideo> queryWrapperwrapper = new QueryWrapper<>();
        queryWrapperwrapper.eq("chapter_id",courseId);
        int count = videoService.count(queryWrapperwrapper);
        if (count>0){
            //查询出小节，不能删除
            throw new GuliException(20001,"不能删除");
        }else {
            //没有小节，可以删除
            int result = baseMapper.deleteById(courseId);
            return result>0;
        }
    }


}
