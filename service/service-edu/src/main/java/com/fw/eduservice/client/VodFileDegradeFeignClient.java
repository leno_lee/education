package com.fw.eduservice.client;

import com.fw.commonutils.R;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VodFileDegradeFeignClient implements VodClient {
    //删除单个视频
    @Override
    public R deleteAlyiVideo(String videoId) {
        return R.ok().message("删除单个视频失败");
    }
    //删除多个视频
    @Override
    public R deleteAlyListVideo(List<String> videoIdList) {
        return R.ok().message("删除多个视频失败");
    }
}
