package com.fw.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VodService {
    //上传视频到阿里云
    String uploadVideo(MultipartFile file);
    //删除阿里云中视频
    void deleteVideo(String videoId);
    //删除阿里云中多个视频
    void deleteListVideo(List<String> videoIdList);
    //获得阿里云视频的凭证
    String getPlayAuth(String videoId);
}
