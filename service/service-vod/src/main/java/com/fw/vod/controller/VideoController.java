package com.fw.vod.controller;

import com.fw.commonutils.R;
import com.fw.servicebase.exception.GuliException;
import com.fw.vod.service.VodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/eduvod/video")
@CrossOrigin
public class VideoController {
    @Autowired
    private VodService vodService;

    //上传视频到阿里云
    @PostMapping("uploadAlyiVideo")
    public R uploadAlyiVideo(MultipartFile file){
        String videoId = vodService.uploadVideo(file);
        return R.ok().data("videoId",videoId);
    }
    //删除阿里云中单个的视频
    @DeleteMapping("deleteAlyiVideo/{videoId}")
    public R deleteAlyiVideo(@PathVariable("videoId") String videoId){
        //删除阿里云中视频
        vodService.deleteVideo(videoId);
        return R.ok();
    }
    //删除阿里云中多个视频
    @DeleteMapping("deleteAlyListVideo")
    public R deleteAlyListVideo(@RequestParam("videoIdList") List<String> videoIdList){
        //删除阿里云中多个视频
        vodService.deleteListVideo(videoIdList);
        return R.ok();
    }
    //3、获得阿里云视频的凭证
    @GetMapping("getPlayAuth/{videoId}")
    public R getPlayAuth(@PathVariable("videoId")String videoId){
        String playAuth = vodService.getPlayAuth(videoId);
        return R.ok().data("playAuth",playAuth);
    }
}
