package com.fw.vod.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.fw.servicebase.exception.GuliException;
import com.fw.vod.service.VodService;
import com.fw.vod.utils.ConstantVodUtil;
import com.fw.vod.utils.InitVodClient;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class VodServiceImpl implements VodService {
    @Override
    public String uploadVideo(MultipartFile file) {
        try {
            //id
            String keyId = ConstantVodUtil.ACCESS_KEY_ID;
            //秘钥
            String keySecret=ConstantVodUtil.ACCESS_KEY_SECRET;

            String fileName = file.getOriginalFilename();
            String title = fileName.substring(0,fileName.lastIndexOf("."));

            InputStream inputStream = file.getInputStream();

            UploadStreamRequest request = new UploadStreamRequest(keyId,keySecret,title,fileName,inputStream);
            UploadVideoImpl uploader = new UploadVideoImpl();
            UploadStreamResponse response = uploader.uploadStream(request);
            String videoId=null;
            if (response.isSuccess()) {
                // System.out.print("VideoId=" + response.getVideoId() + "\n");
                videoId = response.getVideoId();
            }
            return  videoId;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }
    //删除阿里云中视频
    @Override
    public void deleteVideo(String videoId) {
        //删除阿里云中的视频
        DeleteVideoRequest request = new DeleteVideoRequest();
        DeleteVideoResponse response = new DeleteVideoResponse();
        try {
            DefaultAcsClient client = InitVodClient.initVodClient(ConstantVodUtil.ACCESS_KEY_ID, ConstantVodUtil.ACCESS_KEY_SECRET);
            request.setVideoIds(videoId);
            response = client.getAcsResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
            throw new GuliException(20001,"删除视频失败");
        }
    }
    //删除阿里云中多个视频
    @Override
    public void deleteListVideo(List<String> videoIdList) {
        try {
            //初始化客户端
            DefaultAcsClient client = InitVodClient.initVodClient(ConstantVodUtil.ACCESS_KEY_ID, ConstantVodUtil.ACCESS_KEY_SECRET);
            //构造请求和响应
            DeleteVideoRequest request = new DeleteVideoRequest();
            DeleteVideoResponse response = new DeleteVideoResponse();

            //构造setVideoIds所需要的参数形式
            String videoIds = StringUtils.join(videoIdList.toArray(), ",");
            //setVideoIds 此方法可以传递多个视频id值，但必须为一个字符串
            //即，“1,2,3,4”
            request.setVideoIds(videoIds);
            response=client.getAcsResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
            throw new GuliException(20001,"删除多个视频失败");
        }
    }

    //获得阿里云视频的凭证
    @Override
    public String getPlayAuth(String videoId) {
        try {
            //初始化对象
            DefaultAcsClient client = InitVodClient.initVodClient(ConstantVodUtil.ACCESS_KEY_ID,ConstantVodUtil.ACCESS_KEY_SECRET);
            //获得request和response对象
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();

            //设置视频id
            request.setVideoId(videoId);
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);

            String playAuth = response.getPlayAuth();
            return playAuth;
        } catch (ClientException e) {
            throw new GuliException(20001,"获得视频凭证失败");
        }
    }
}
