package com.fw.staservice.service;

import com.fw.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-24
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {
    //某天的注册人数统计
    void dailyRegister(String day);
    //统计报表的数据
    Map<String, Object> getShowData(String type, String begin, String end);
}
