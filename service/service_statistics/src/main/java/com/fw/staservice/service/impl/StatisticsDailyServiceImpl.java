package com.fw.staservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.commonutils.R;
import com.fw.staservice.client.StaClient;
import com.fw.staservice.entity.StatisticsDaily;
import com.fw.staservice.mapper.StatisticsDailyMapper;
import com.fw.staservice.service.StatisticsDailyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-24
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    private StaClient staClient;

    @Override
    public void dailyRegister(String day) {
        //删除原先的记录
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.eq("date_calculated",day);
        baseMapper.delete(wrapper);

        R r = staClient.registerCount(day);
        Integer count = (Integer) r.getData().get("count");
        Integer loginNum = RandomUtils.nextInt(100, 200);//TODO
        Integer videoViewNum = RandomUtils.nextInt(100, 200);//TODO
        Integer courseNum = RandomUtils.nextInt(100, 200);//TODO

        StatisticsDaily sta = new StatisticsDaily();
        sta.setRegisterNum(count);
        sta.setLoginNum(loginNum);
        sta.setVideoViewNum(videoViewNum);
        sta.setCourseNum(courseNum);
        sta.setDateCalculated(day);

        baseMapper.insert(sta);
    }

    /**
     *数据统计报表的数据
     * @param type 统计因子，根据什么来统计
     * @param begin 开始时间
     * @param end 截止时间
     * @return map集合
     */
    @Override
    public Map<String, Object> getShowData(String type, String begin, String end) {
        //查询数据
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.select("date_calculated",type);
        wrapper.between("date_calculated",begin,end);
        List<StatisticsDaily> statisticsDailies = baseMapper.selectList(wrapper);

        //由于Echarts中的数据需要数组形式，封装日期数据和数量数据
        List<String> date_calculatedList = new ArrayList<>();
        List<Integer> numList = new ArrayList<>();

        for (StatisticsDaily statisticsDaily : statisticsDailies) {
            //封装日期数据
            date_calculatedList.add(statisticsDaily.getDateCalculated());
            //封装数量数据
            switch (type){
                case "login_num":
                    numList.add(statisticsDaily.getLoginNum());
                    break;
                case "register_num":
                    numList.add(statisticsDaily.getRegisterNum());
                    break;
                case "video_view_num":
                    numList.add(statisticsDaily.getVideoViewNum());
                    break;
                case "course_num":
                    numList.add(statisticsDaily.getCourseNum());
                    break;
                default:
                    break;
            }
        }

        //封装成map集合
        Map<String,Object> map = new HashMap<>();
        map.put("date_calculatedList",date_calculatedList);
        map.put("numList",numList);

        return map;
    }
}
