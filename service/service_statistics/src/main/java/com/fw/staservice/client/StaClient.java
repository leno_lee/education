package com.fw.staservice.client;

import com.fw.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "service-ucenter")
public interface StaClient {
    @GetMapping("/eduucenter/member/registerCount/{day}")
    public R registerCount(@PathVariable("day") String day);
}
