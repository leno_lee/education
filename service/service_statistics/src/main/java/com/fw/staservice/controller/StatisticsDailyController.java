package com.fw.staservice.controller;


import com.fw.commonutils.R;
import com.fw.staservice.service.StatisticsDailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-24
 */
@RestController
@RequestMapping("/staservice/sta")
@CrossOrigin
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService staService;

    @GetMapping("dailyRegister/{day}")
    public R dailyRegister(@PathVariable("day") String day){
        staService.dailyRegister(day);
        return R.ok();
    }

    /**
     * 数据统计报表的数据
     * @param type 统计因子，根据什么来统计
     * @param begin 开始时间
     * @param end 截止时间
     * @return map集合
     */
    @GetMapping("showData/{type}/{begin}/{end}")
    public R showData(@PathVariable("type") String type,
                      @PathVariable("begin") String begin,
                      @PathVariable("end") String end){
        Map<String,Object> map = staService.getShowData(type,begin,end);
        return R.ok().data(map);
    }
}

