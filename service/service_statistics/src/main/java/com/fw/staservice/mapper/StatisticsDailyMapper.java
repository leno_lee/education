package com.fw.staservice.mapper;

import com.fw.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-24
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
