package com.fw.staservice.schedule;

import com.fw.staservice.service.StatisticsDailyService;
import com.fw.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ScheduledTask {
    @Autowired
    private StatisticsDailyService staService;

    @Scheduled(cron = "0 0 1 * * ?")
    public void task2(){
        staService.dailyRegister(DateUtil.formatDate(DateUtil.addDays(new Date(),-1)));
    }
}
