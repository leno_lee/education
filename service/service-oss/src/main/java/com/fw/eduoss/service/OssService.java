package com.fw.eduoss.service;

import org.springframework.web.multipart.MultipartFile;

public interface OssService {
    /**
     * 文件上传到阿里云
     * MultipartFile用于文件上传
     */
    String upload(MultipartFile file);
}
