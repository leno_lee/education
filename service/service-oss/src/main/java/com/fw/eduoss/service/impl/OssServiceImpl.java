package com.fw.eduoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.fw.eduoss.service.OssService;
import com.fw.eduoss.utils.ConstantPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class OssServiceImpl implements OssService {
    @Override
    public String upload(MultipartFile file) {
        String endpoint = ConstantPropertiesUtils.END_POINT;
        String accessKeyId = ConstantPropertiesUtils.KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.KEY_SECRET;
        String bucketName = ConstantPropertiesUtils.BUCKET_NAME;
        String uploadUrl = null;
        try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 获取上传文件流。
            InputStream inputStream = file.getInputStream();
            //获取文件名称
            String filename = file.getOriginalFilename();
            //UUID
            filename = UUID.randomUUID().toString().replace("-","")+filename;
            //构建日期路径：avatar/2019/02/26/文件名
            String filePath = new DateTime().toString("yy/MM/dd");
            filename = filePath + filename;
            //第二个参数 上传到oss文件路径和文件名， /aa/bb/1.jpg
            ossClient.putObject(bucketName, filename, inputStream);

            // 关闭OSSClient。
            ossClient.shutdown();

            //手动拼接url
            //https://guli20927.oss-cn-beijing.aliyuncs.com/mm.jpg
            uploadUrl = "https://"+bucketName+"."+endpoint+"/"+filename;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return uploadUrl;
    }
}
