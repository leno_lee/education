package com.fw.eduoss.controller;




import com.fw.commonutils.R;
import com.fw.eduoss.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(description = "阿里云文件管理")
@RestController
@RequestMapping("/eduoss/fileoss")
@CrossOrigin
public class FileUploadController {

    @Autowired
    private OssService ossService;

    //上传头像的接口
    @ApiOperation(value = "文件上传")
    @PostMapping
    public R upload(MultipartFile file){
        String url = ossService.upload(file);
        return R.ok().data("url",url);
    }
}
