package com.fw.eduoss.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
/**
 * 继承InitializingBean接口，在属性初始化后执行操作
 * 对外提供一些公共的常量
 */
public class ConstantPropertiesUtils implements InitializingBean {
    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint; //阿里云endpoint

    @Value("${aliyun.oss.file.keyid}")
    private String keyId;   //阿里云OSS的keyId

    @Value("${aliyun.oss.file.keysecret}")
    private String keySecret;   //OSS的秘钥

    @Value("${aliyun.oss.file.bucketname}")
    private String bucketName;  //OSS bucket的名字

    public static String END_POINT;
    public static String KEY_ID;
    public static String KEY_SECRET;
    public static String BUCKET_NAME;

    @Override
    public void afterPropertiesSet() throws Exception {

        END_POINT = this.endpoint;
        KEY_ID = this.keyId;
        KEY_SECRET = this.keySecret;
        BUCKET_NAME = this.bucketName;
    }
}
