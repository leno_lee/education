package com.fw.msmservice.controller;

import com.fw.commonutils.R;
import com.fw.msmservice.service.MsmService;
import com.fw.msmservice.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/edumsm/msm")
@CrossOrigin
public class MsmController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    //发送验证码
    @GetMapping("send/{phone}")
    public R send(@PathVariable("phone") String phone){
        //验证redis中是否有验证码
        String code1 = redisTemplate.opsForValue().get("phone");
        if (!StringUtils.isEmpty(code1)){
            return R.ok();
        }
        //创建code验证码
        String code = RandomUtil.getFourBitRandom();
        Map<String,Object> param = new HashMap<>();
        param.put("code",code);
        boolean isSend = msmService.send(param,phone);
        //如果成功将其放入redis中
        if (isSend){
            //设置过期时间
            redisTemplate.opsForValue().set(phone,code,5, TimeUnit.MINUTES);
            return R.ok();
        }else {
            return R.error().message("发送短信失败");
        }
    }
}
