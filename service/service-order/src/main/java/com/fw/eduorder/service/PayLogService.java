package com.fw.eduorder.service;

import com.fw.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
public interface PayLogService extends IService<PayLog> {
    //生成二维码的方法
    Map<String, Object> createNative(String orderNo);
    //查询支付状态
    Map<String, String> queryPayStatus(String orderNo);
    //更改支付状态
    void updateOrderStatus(Map<String, String> map);

}
