package com.fw.eduorder.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.eduorder.entity.Order;
import com.fw.eduorder.entity.PayLog;
import com.fw.eduorder.mapper.PayLogMapper;
import com.fw.eduorder.service.OrderService;
import com.fw.eduorder.service.PayLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.eduorder.utils.HttpClient;
import com.fw.eduorder.utils.WXConstant;
import com.fw.servicebase.exception.GuliException;
import com.github.wxpay.sdk.WXPayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.MarshalledObject;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 支付日志表 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
@Service
public class PayLogServiceImpl extends ServiceImpl<PayLogMapper, PayLog> implements PayLogService {

    @Autowired
    private OrderService orderService;

    /**
     * 生成二维码
     * @param orderNo 订单号
     * @return
     */
    @Override
    public Map<String, Object> createNative(String orderNo) {
        try {
            //查询订单
            QueryWrapper<Order> wrapper = new QueryWrapper<>();
            wrapper.eq("order_no",orderNo);
            Order order = orderService.getOne(wrapper);
            //设置参数
            Map<String,String> m = new HashMap<>();
            m.put("appid", WXConstant.WX_APPID);
            m.put("mch_id",WXConstant.PARTNER);
            m.put("nonce_str", WXPayUtil.generateNonceStr());
            m.put("body", order.getCourseTitle());
            m.put("out_trade_no", orderNo);
            m.put("total_fee", order.getTotalFee().multiply(new BigDecimal("100")).longValue()+"");
            m.put("spbill_create_ip", "127.0.0.1");
            m.put("notify_url", WXConstant.UNIFIED_URL+"\n");
            m.put("trade_type", "NATIVE");

            //向特定url发送请求
            HttpClient client = new HttpClient(WXConstant.UNIFIED_URL);
            client.setXmlParam(WXPayUtil.generateSignedXml(m,WXConstant.PARTNER_KEY));
            client.setHttps(true);
            client.post();

            //得到返回数据
            String xml = client.getContent();
            Map<String,String> resultMap = WXPayUtil.xmlToMap(xml);
            //封装数据
            Map<String,Object> map = new HashMap<>();
            map.put("out_trade_no", orderNo);
            map.put("course_id", order.getCourseId());
            map.put("total_fee", order.getTotalFee());
            map.put("result_code", resultMap.get("result_code"));
            map.put("code_url", resultMap.get("code_url"));

            return map;
        } catch (Exception e) {
            throw new GuliException(20001,"生成二维码失败");
        }
    }

    /**
     * 向微信官方查询订单状态，即是否被支付
     * @param orderNo
     * @return
     */
    @Override
    public Map<String, String> queryPayStatus(String orderNo) {
        try {
            //1、封装参数
            Map<String,String> m = new HashMap<>();
            m.put("appid", WXConstant.WX_APPID);
            m.put("mch_id", WXConstant.PARTNER);
            m.put("out_trade_no", orderNo);
            m.put("nonce_str", WXPayUtil.generateNonceStr());

            //2、设置请求
            HttpClient client = new HttpClient(WXConstant.ORDER_QUERY);
            client.setXmlParam(WXPayUtil.generateSignedXml(m,WXConstant.PARTNER_KEY));
            client.setHttps(true);
            client.post();

            //3、得到返回数据
            String xml = client.getContent();
            Map<String,String> resultMap = WXPayUtil.xmlToMap(xml);

            //4、封装返回数据
            return resultMap;
        } catch (Exception e) {
            throw new GuliException(20001,"查询订单支付状态失败");
        }
    }

    /**
     * 更改支付状态
     * @param map
     */
    @Override
    public void updateOrderStatus(Map<String, String> map) {
        //获取订单id
        String orderNo = map.get("out_trade_no");

        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("order_no",orderNo);
        Order order = orderService.getOne(wrapper);

        if (order.getStatus().intValue() == 1) return;
        order.setStatus(1);
        orderService.updateById(order);

        //添加记录
        PayLog payLog = new PayLog();
        payLog.setOrderNo(order.getOrderNo());//支付订单号
        payLog.setPayTime(new Date());
        payLog.setPayType(1);//支付类型
        payLog.setTotalFee(order.getTotalFee());//总金额(分)
        payLog.setTradeState(map.get("trade_state"));//支付状态
        payLog.setTransactionId(map.get("transaction_id"));
        payLog.setAttr(JSONObject.toJSONString(map));
        baseMapper.insert(payLog);//插入到支付日志表
    }


}
