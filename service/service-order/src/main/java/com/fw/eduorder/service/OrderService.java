package com.fw.eduorder.service;

import com.fw.eduorder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
public interface OrderService extends IService<Order> {
    //生成订单
    String saveOrder(String courseId, String memberId);
    //查询指定用户的支付记录中是否有指定课程
    boolean isBuyCourse(String courseId, String memberId);
}
