package com.fw.eduorder.mapper;

import com.fw.eduorder.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
public interface OrderMapper extends BaseMapper<Order> {

}
