package com.fw.eduorder.mapper;

import com.fw.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
