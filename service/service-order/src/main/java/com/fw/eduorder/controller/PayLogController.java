package com.fw.eduorder.controller;


import com.fw.commonutils.JwtUtils;
import com.fw.commonutils.R;
import com.fw.eduorder.service.OrderService;
import com.fw.eduorder.service.PayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/eduorder/payLog")
@CrossOrigin
public class PayLogController {

    @Autowired
    private PayLogService payLogService;

    /**
     * 生成二维码
     * @param orderNo 订单号
     * @return 微信给的信息
     */
    @GetMapping("createNative/{orderNo}")
    public R createNative(@PathVariable("orderNo") String orderNo){
        Map<String,Object> map = payLogService.createNative(orderNo);
        return R.ok().data(map);
    }

    /**
     * 查询支付状态
     * @param orderNo 订单号
     * @return 是否支付完成
     */
    @GetMapping("queryPayStatus/{orderNo}")
    public R queryPayStatus(@PathVariable("orderNo") String orderNo){
        //查询支付状态
        Map<String,String> map = payLogService.queryPayStatus(orderNo);
        if (map == null){
            return R.error().message("支付出错");
        }

        if (map.get("trade_state").equals("SUCCESS")){
            //更改支付状态
            payLogService.updateOrderStatus(map);
            return R.ok().message("支付成功");
        }
        return R.ok().code(25000).message("支付中");
    }


}

