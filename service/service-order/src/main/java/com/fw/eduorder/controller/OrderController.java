package com.fw.eduorder.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.commonutils.CourseOrderInfoVo;
import com.fw.commonutils.JwtUtils;
import com.fw.commonutils.R;
import com.fw.commonutils.UcenterMemberVo;
import com.fw.eduorder.client.EduClient;
import com.fw.eduorder.client.UserClient;
import com.fw.eduorder.entity.Order;
import com.fw.eduorder.service.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
@RestController
@RequestMapping("/eduorder/order")
@CrossOrigin
public class OrderController {
    @Autowired
    private OrderService orderService;

    //添加订单
    @PostMapping("addOrder/{courseId}")
    public R addOder(@PathVariable("courseId") String courseId,
                     HttpServletRequest request){
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        String orderNo = orderService.saveOrder(courseId,memberId);
        return R.ok().data("orderNo",orderNo);
    }

    //根据订单号获得订单信息
    @GetMapping("getOrder/{orderNo}")
    public R getOrder(@PathVariable("orderNo") String orderNo){
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("order_no",orderNo);
        Order order = orderService.getOne(wrapper);
        return R.ok().data("item",order);
    }


    /**
     * 判断该课程是否被该用户所购买
     * @param courseId 课程id
     * @return true 已购买，false 未购买
     */
    @GetMapping("isBuyCourse/{courseId}/{memberId}")
    public boolean isBuyCourse(@PathVariable("courseId") String courseId,
                               @PathVariable("memberId") String memberId){
        return orderService.isBuyCourse(courseId,memberId);
    }
}

