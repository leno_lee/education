package com.fw.eduorder.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WXConstant implements InitializingBean {

    @Value("${weixin.pay.appid}")
    private String WxAppId;

    @Value("${weixin.pay.partner}")
    private String partner;

    @Value("${weixin.pay.partnerkey}")
    private String partnerKey;

    @Value("${weixin.pay.notifyurl}")
    private String notifyUrl;

    @Value("${weixin.pay.unifiedurl}")
    private String unifiedUrl;

    @Value("${weixin.pay.orderqueryurl}")
    private String orderQuery;

    public static String WX_APPID;
    public static String PARTNER;
    public static String PARTNER_KEY;
    public static String NOTIFY_URL;
    public static String UNIFIED_URL;
    public static String ORDER_QUERY;

    @Override
    public void afterPropertiesSet() throws Exception {
        WX_APPID = this.WxAppId;
        PARTNER = this.partner;
        PARTNER_KEY = this.partnerKey;
        NOTIFY_URL = this.notifyUrl;
        UNIFIED_URL = this.unifiedUrl;
        ORDER_QUERY = this.orderQuery;
    }
}
