package com.fw.eduorder.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fw.commonutils.CourseOrderInfoVo;
import com.fw.commonutils.UcenterMemberVo;
import com.fw.eduorder.client.EduClient;
import com.fw.eduorder.client.UserClient;
import com.fw.eduorder.entity.Order;
import com.fw.eduorder.entity.PayLog;
import com.fw.eduorder.mapper.OrderMapper;
import com.fw.eduorder.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.eduorder.utils.HttpClient;
import com.fw.eduorder.utils.OrderNoUtil;
import com.fw.eduorder.utils.WXConstant;
import com.fw.servicebase.exception.GuliException;
import com.github.wxpay.sdk.WXPayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author fw
 * @since 2020-10-21
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private EduClient eduClient;

    @Autowired
    private UserClient userClient;

    @Override
    public String saveOrder(String courseId, String memberId) {
        //获得课程信息
        CourseOrderInfoVo courseDto = eduClient.getOrderCourseInfo(courseId);
        //获得用户信息
        UcenterMemberVo ucenterMember = userClient.getUserInfo(memberId);

        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseDto.getTitle());
        order.setCourseCover(courseDto.getCover());
        order.setTeacherName("test");
        order.setTotalFee(courseDto.getPrice());
        order.setMemberId(memberId);
        order.setMobile(ucenterMember.getMobile());
        order.setNickname(ucenterMember.getNickname());
        order.setStatus(0);
        order.setPayType(1);
        baseMapper.insert(order);

        return order.getOrderNo();
    }
    /**
     * 判断该课程是否被该用户所购买
     * @param courseId 课程id
     * @param memberId 用户id
     * @return true 已购买，false 未购买
     */
    @Override
    public boolean isBuyCourse(String courseId, String memberId) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        wrapper.eq("member_id",memberId);
        Order order = baseMapper.selectOne(wrapper);


        return order.getStatus().intValue()>0;
    }
}
