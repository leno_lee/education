package com.fw.eduorder.client;

import com.fw.commonutils.CourseOrderInfoVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name ="service-edu" )
public interface EduClient {
    //订单获得课程信息
    @GetMapping("/eduservice/coursefront/getOrderCourseInfo/{courseId}")
    public CourseOrderInfoVo getOrderCourseInfo(@PathVariable("courseId") String courseId);
}
