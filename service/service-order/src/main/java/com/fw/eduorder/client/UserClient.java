package com.fw.eduorder.client;

import com.fw.commonutils.UcenterMemberVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "service-ucenter")
public interface UserClient {
    @GetMapping("/eduucenter/member/getUserInfo/{id}")
    public UcenterMemberVo getUserInfo(@PathVariable("id") String id);
}
