package com.fw.cmsservice.controller;

import com.fw.cmsservice.entity.CrmBanner;
import com.fw.cmsservice.service.CrmBannerService;
import com.fw.commonutils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/educms/banner")
@CrossOrigin
public class BannerFrontController {
    @Autowired
    private CrmBannerService bannerService;

    @GetMapping("getAllBanner")
    public R getAllBanner(){
        //获得所有的幻灯片
        List<CrmBanner> list = bannerService.selectAllBanner();
        return R.ok().data("bannerList",list);
    }
}
