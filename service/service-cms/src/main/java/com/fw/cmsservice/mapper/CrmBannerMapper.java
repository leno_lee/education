package com.fw.cmsservice.mapper;

import com.fw.cmsservice.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author fw
 * @since 2020-10-15
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
