package com.fw.cmsservice.service;

import com.fw.cmsservice.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author fw
 * @since 2020-10-15
 */
public interface CrmBannerService extends IService<CrmBanner> {
    //获得所有的幻灯片
    List<CrmBanner> selectAllBanner();
}
